#include <boost/preprocessor/control/while.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/seq/for_each_i.hpp>
#include <boost/preprocessor/cat.hpp>
#include <stddef.h>
typedef void (*handler_t)(void);

#define HANDLER_ID(name) BOOST_PP_CAT(name, _ID) 
#define HANDLER_FUNC(name) BOOST_PP_CAT(name, _func)

#define E_TO_ENUM(_1, _2, id, n) HANDLER_ID(n)=id,
#define LIST_TO_ENUM(...) BOOST_PP_SEQ_FOR_EACH_I(E_TO_ENUM, , BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define E_TO_DECL(_1, _2, _3, n) extern void HANDLER_FUNC(n)();
#define LIST_TO_DECLS(...) BOOST_PP_SEQ_FOR_EACH_I(E_TO_DECL, , BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define E_TO_ARR_ELEM(_1, _2, _3, n) HANDLER_FUNC(n), 
#define LIST_TO_ARRAY(...) BOOST_PP_SEQ_FOR_EACH_I(E_TO_ARR_ELEM, , BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#define DECLARE_HANDLER(name) void HANDLER_FUNC(name)(void)

#ifdef __HANDLERS_C
#define DECLARE_HANDLERS(...) \
	LIST_TO_DECLS(__VA_ARGS__) \
	enum handlers_e {LIST_TO_ENUM(__VA_ARGS__) NUM_HANDLERS}; \
	handler_t handlers[] = {LIST_TO_ARRAY(__VA_ARGS__) NULL};
#else 
#define DECLARE_HANDLERS(...) \
	LIST_TO_DECLS(__VA_ARGS__) \
	enum handlers_e {LIST_TO_ENUM(__VA_ARGS__) NUM_HANDLERS};
#endif
