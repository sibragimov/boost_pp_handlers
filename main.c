#include "handlers.h"

int main() {
	call_by_id(HANDLER_ID(func_x));
	call_by_id(HANDLER_ID(func_y));
	call_by_id(HANDLER_ID(abc));
	call_by_id(0);
	call_by_id(1);
	call_by_id(2);
	call_by_id(3);
	return 0;
}
 /*Output:
$./test
func_x
func_y
abc
func_x
func_y
abc
Error! Illegal handler number
*/
