#define __HANDLERS_C
#include "handlers.h"
#include <stdio.h>

void call_by_id(int n) {
	if(n>=NUM_HANDLERS) {
		printf("Error! Illegal handler number\n");
		return;
	} else {
		return handlers[n]();
	}
}
