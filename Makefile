OBJ = handlers.o handlers_impl.o main.o
HEADERS = handlers.h  handlers_internal.h

%.o : %.c $(HEADERS)
	gcc  -c -o $@ $<

test: $(OBJ) 
	gcc -o $@ $^

