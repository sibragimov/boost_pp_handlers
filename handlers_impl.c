#include "handlers.h"
#include "stdio.h"

DECLARE_HANDLER(func_x) {
	printf("func_x\n");
}

DECLARE_HANDLER(func_y) {
	printf("func_y\n");
}

DECLARE_HANDLER(abc) {
	printf("abc\n");
}
